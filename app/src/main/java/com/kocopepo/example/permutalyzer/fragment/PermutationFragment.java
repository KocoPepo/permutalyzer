package com.kocopepo.example.permutalyzer.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.kocopepo.example.permutalyzer.R;
import com.kocopepo.example.permutalyzer.adapter.PermutationAdapter;
import com.kocopepo.example.permutalyzer.commons.SimpleDividerItemDecoration;
import com.kocopepo.example.permutalyzer.math.Permutation;

import java.text.NumberFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author kocopepo
 * created on 26.01.16
 */
public class PermutationFragment extends Fragment {

    private static final String TAG = PermutationFragment.class.getSimpleName();
    private NumberFormat mNumberFormat;
    private PermutationAdapter mAdapter;

    @Bind(R.id.all_permutations_count) TextView allPermutationsCount;
    @Bind(R.id.source_to_permute) EditText sourceEditText;
    @Bind(R.id.all_permutations) RecyclerView allPermutations;

    public PermutationFragment() {
        mNumberFormat = NumberFormat.getNumberInstance();
    }
    public static Fragment newInstance() {
        return new PermutationFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mAdapter = new PermutationAdapter(new Permutation(""));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_permutation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        allPermutations.setHasFixedSize(true);
        allPermutations.setLayoutManager(new LinearLayoutManager(getContext()));
        allPermutations.setAdapter(mAdapter);
    }

    @OnClick(R.id.permute_button)
    public void onClickPermute() {
        Permutation permutation = new Permutation(sourceEditText.getText().toString());
        mAdapter = new PermutationAdapter(permutation);
        allPermutations.setAdapter(mAdapter);
        allPermutations.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        allPermutationsCount.setText(mNumberFormat.format(permutation.countAllPermutations()));
    }
}
