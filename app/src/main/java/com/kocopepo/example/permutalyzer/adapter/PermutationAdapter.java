package com.kocopepo.example.permutalyzer.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kocopepo.example.permutalyzer.R;
import com.kocopepo.example.permutalyzer.math.Permutation;

import java.math.BigInteger;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author kocopepo
 * created on 26.01.16
 */
public class PermutationAdapter extends RecyclerView.Adapter<PermutationAdapter.ViewHolder> {

    private Permutation mPermutation;
    private int mLastPosition;
    private int mItemCount;

    public PermutationAdapter(@NonNull Permutation permutation) {
        mPermutation = permutation;
        mLastPosition = 0;
        BigInteger bigInteger = permutation.countAllPermutations();
        mItemCount = bigInteger.intValue();
        if (bigInteger.bitLength() >= 32) {
            mItemCount = Integer.MAX_VALUE;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_permutation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        while (mLastPosition != position) {
            if (mLastPosition > position) {
                mPermutation.previous();
                mLastPosition --;
            }
            else {
                mPermutation.next();
                mLastPosition++;
            }
        }
        holder.permutation.setText(mPermutation.toString());
    }

    @Override
    public int getItemCount() {
        return mItemCount;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.permutation) TextView permutation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
