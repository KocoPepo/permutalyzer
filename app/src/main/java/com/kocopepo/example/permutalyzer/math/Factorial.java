package com.kocopepo.example.permutalyzer.math;

import java.math.BigInteger;

/**
 * @author kocopepo
 * created on 16.01.16
 */
public class Factorial {
    private int mBase;
    private int mEnd;

    public Factorial(int base, int end) {
        mBase = base;
        mEnd = end;
    }

    public Factorial(int base) {
        this(base, 0);
    }

    public BigInteger getFactorial() {
        return fact(mBase, mEnd);
    }

    private BigInteger fact(int start, int end) {
        if(start <= 1) {
            return BigInteger.ONE;
        }
        BigInteger factorial = new BigInteger("1");
        while (start > end && start > 1) {
            factorial = factorial.multiply(BigInteger.valueOf(start));
            start--;
        }
        return factorial;
    }
}
