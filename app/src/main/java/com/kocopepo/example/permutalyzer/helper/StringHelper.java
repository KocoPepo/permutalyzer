package com.kocopepo.example.permutalyzer.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author kocopepo
 * created on 16.01.16
 */
public class StringHelper {
    public static List<Integer> getCountArrayOfCharOccurrence(String string) {
        string = sort(string);
        int count = 1;
        List<Integer> list = new ArrayList<>();
        for(int i = 1; i < string.length(); i++) {
            if(string.charAt(i) == string.charAt(i-1)) {
                count++;
            }
            else {
                if(count > 1){
                    list.add(count);
                }
                count = 1;
            }
        }
        if(count > 1) {
            list.add(count);
        }
        return list;
    }

    public static String sort(String string) {
        char[] chars = string.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}
