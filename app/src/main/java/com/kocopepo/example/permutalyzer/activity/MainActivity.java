package com.kocopepo.example.permutalyzer.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kocopepo.example.permutalyzer.fragment.PermutationFragment;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(android.R.id.content, PermutationFragment.newInstance())
                    .commit();
        }
    }
}
