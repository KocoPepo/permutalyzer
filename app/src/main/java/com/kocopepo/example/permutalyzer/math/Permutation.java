package com.kocopepo.example.permutalyzer.math;

import com.kocopepo.example.permutalyzer.helper.StringHelper;

import java.math.BigInteger;

/**
 * @author Martin Kocour
 */
public class Permutation {
    private String mLast;

    public Permutation(String sourceToPermute, boolean sort) {
        if(sort) {
            sourceToPermute = StringHelper.sort(sourceToPermute);
        }
        mLast = sourceToPermute;
    }

    @Override
    public String toString() {
        return mLast;
    }

    public Permutation(String sourceToPermute) {
        this(sourceToPermute, true);
    }

    public String getLast() {
        return mLast;
    }

    /**
     * Count all possible permutations without repetition.
     * Mathematical formula is:
     * N!/productOf(char1Occurence!...charNOccurence!)
     *
     * @return BigInteger of all possible permutations without repetition
     */
    public BigInteger countAllPermutations() {
        if(mLast.length() == 0) {
            return BigInteger.ZERO;
        }
        Factorial factorial = new Factorial(mLast.length());
        BigInteger product = BigInteger.ONE;
        for (int i : StringHelper.getCountArrayOfCharOccurrence(mLast)) {
            product = product.multiply(new Factorial(i).getFactorial());
        }
        return factorial.getFactorial().divide(product);
    }

    public boolean hasNext() {
        boolean isLast = findLargestK(mLast.toCharArray()) == -1;
        return !isLast;
    }

    public boolean hasPrevious() {
        boolean isFirst = findLargestM(mLast.toCharArray()) == -1;
        return !isFirst;
    }

    public String next() {
        if(hasNext()) {
            return mLast = next(mLast);
        }
        return null;
    }

    public String previous() {
        if (hasPrevious()) {
            return mLast = previous(mLast);
        }
        return null;
    }


    /**
     * Generates the next permutation lexicographically after a given permutation.
     *
     * The following algorithm generates the next permutation lexicographically
     * after a given permutation. It changes the given permutation in-place.
     * 1.   Find the largest index k such that a[k] < a[k + 1]. If no such index
     *      exists, the permutation is the last permutation.
     * 2.   Find the largest index l greater than k such that a[k] < a[l].
     * 3.   Swap the value of a[k] with that of a[l].
     * 4.   Reverse the sequence from a[k + 1] up to and including the final element a[n].
     * @return the next permutation
     */
    private String next(String previousPermutation) {
        char[] a = previousPermutation.toCharArray();
        int k = findLargestK(a);
        if(k == -1) {
            return null;
        }
        int l = findLargestL(a, k);
        /* Swap the value of a[k] with that of a[l]  */
        char tmp = a[k];
        a[k] = a[l];
        a[l] = tmp;
        /* Reverse the sequence from a[k + 1] up to and including the final element a[n] */
        StringBuilder whole = new StringBuilder(new String(a));
        StringBuilder toReverse = new StringBuilder(whole.substring(k+1));
        toReverse.reverse();
        whole = whole.replace(k+1, whole.length(), toReverse.toString());
        return whole.toString();
    }

    /**
     * Find the largest index k such that a[k] < a[k + 1]. If no such index
     * exists, the permutation is the last permutation and method returns -1.
     *
     * @param a char array
     * @return largest index if it exists. Otherwise -1 is returned.
     */
    private int findLargestK(char[] a) {
        int k = -1;
        for(int i = 0; i < a.length - 1; i++) {
            if (a[i] < a[i+1]) {
                k = i;
            }
        }
        return k;
    }

    /**
     * Find the largest index l greater than k such that a[k] < a[l].
     *
     * @param a char array
     * @param k index
     * @return largest index l greater than k
     */
    private int findLargestL(char[] a, int k) {
        int l = k + 1;
        for (int i = l; i < a.length; i++) {
            if(a[k] < a[i]) {
                l = i;
            }
        }
        return l;
    }

    /**
     * Generates the previous permutation lexicographically before a given permutation.
     *
     * The following algorithm generates the previous permutation lexicographically
     * before a given permutation. It changes the given permutation in-place.
     * 1.   Find the largest index m (start from the back) such that a[m] > a[m + 1]. If no such index
     *      exists, the permutation is the first permutation.
     * 2.   Find the largest index n (also start from back) greater than m such that a[n] < a[m].
     * 3.   Swap the value of a[m] with that of a[n].
     * 4.   Reverse the sequence from a[m + 1] up to and including the final element a[n].
     * @return the previous permutation
     */
    private String previous(String nextPermutation) {
        char[] a = nextPermutation.toCharArray();
        int m = findLargestM(a);
        if(m == -1) {
            return null;
        }
        int n = findLargestN(a, m);
        /* Swap the value of a[k] with that of a[l]  */
        char tmp = a[m];
        a[m] = a[n];
        a[n] = tmp;
        /* Reverse the sequence from a[k + 1] up to and including the final element a[n] */
        StringBuilder whole = new StringBuilder(new String(a));
        StringBuilder toReverse = new StringBuilder(whole.substring(m+1));
        toReverse.reverse();
        whole = whole.replace(m+1, whole.length(), toReverse.toString());
        return whole.toString();
    }

    /**
     * Find the largest index m such that a[m] > a[m + 1]. If no such index
     * exists, the permutation is the first permutation.
     *
     * @param a char array
     * @return largest index m or -1, if there is no such index
     */
    private int findLargestM(char[] a) {
        int m = a.length - 2;
        while (m >= 0 && a[m] <= a[m+1]) {
            m--;
        }
        return m;
    }

    /**
     * Find the largest index n greater than m such that a[n] < a[m].
     *
     * @param a char array
     * @param m index
     * @return largest index n
     */
    private int findLargestN(char[] a, int m) {
        int n = a.length - 1;
        while (n > m && a[n] >= a[m]) {
            n --;
        }
        return n;
    }
}
